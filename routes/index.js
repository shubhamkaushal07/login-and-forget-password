var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/test', function(req, res, next) {
  res.render('test');
});
router.get('/recover', function(req, res, next) {
  res.render('recover');
});
router.get('/login', function(req, res, next) {
  res.render('login');
});



module.exports = router;